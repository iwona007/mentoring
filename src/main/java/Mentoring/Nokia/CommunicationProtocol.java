package Mentoring.Nokia;

public abstract class CommunicationProtocol {

    abstract void establishPhysicalLayer();

    abstract void establishDataLinkLayer();

    abstract void establishApplicationLayer();

    // Template method
    public final void Layer() {

        establishApplicationLayer();
        establishDataLinkLayer();
        establishPhysicalLayer();

    }

}
