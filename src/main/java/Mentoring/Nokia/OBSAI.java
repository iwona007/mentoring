package Mentoring.Nokia;

public class OBSAI extends CommunicationProtocol{


    @Override
    void establishPhysicalLayer(){
        System.out.println("Communication protocol named OBSAI has been established by Physical Layer method.");
    }

    @Override
    void establishDataLinkLayer(){
        System.out.println("Communication protocol named OBSAI has been established by Data Link Layer method.");
    }

    @Override
    void establishApplicationLayer(){
        System.out.println("Communication protocol named OBSAI has been established by Application Loyer method.");
    }

}
